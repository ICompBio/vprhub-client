# README #

The ICB VPR HUB introduces a novel new technology that enables the various participating State Central Registries to detect duplicate reported primaries between the registries within a HIPAA compliant de-identified framework. Additionally, the HIPAA compliant de-identified framework will also provide information for the states to know which cases are duplicate reported in another registry. This will allow the Central Registries to enact their processes to clearly adjudicate the case reporting between registries.

Further details, and specific directions on the use of this site are currently provided by directly contacting the ICB Team at Case Western Reserve University.