#!/usr/bin/python
# -*- coding: utf-8 -*-

"""

Written for Case School of Medicine -
Institute for Computational Biology
John P. Turnbull, Ph.D.
Dec. 30, 2015
"""

import sys
import os
import stat
import pymysql
import pymssql

from os import listdir
from os.path import isfile, join

from PyQt4 import QtGui, QtCore


class Matching_MainWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(Matching_MainWindow, self).__init__()
        
        self.initUI()
        
    def initUI(self):      

        Upload_Button = QtGui.QPushButton("Upload Data", self)
        Upload_Button.move(30, 30)

        Match_Button = QtGui.QPushButton("Match Data", self)
        Match_Button.move(150, 30)

        ReIdentify_Button = QtGui.QPushButton("Re-Identify", self)
        ReIdentify_Button.move(270, 30)

        Quit_Button = QtGui.QPushButton("Quit", self)
        Quit_Button.move(390, 30)

	self.Master_ID_input = QtGui.QLineEdit(self)
        self.Master_ID_input.move(270, 70)

	self.Files_to_Process_List = QtGui.QListWidget(self)
	self.Files_to_Process_List.move(30, 140)
	self.Files_to_Process_List.resize(200, 200)

	ftp_label = QtGui.QLabel(self)
	ftp_label.move(30, 100)
	ftp_label.setText("Files to Process")

	self.Processed_Files_List = QtGui.QListWidget(self)
	self.Processed_Files_List.move(270, 140)
	self.Processed_Files_List.resize(200, 200)

	ftp_label = QtGui.QLabel(self)
	ftp_label.move(270, 100)
	ftp_label.setText("Processed Files")

	files_to_be_processed = [files_to_process for files_to_process in
		listdir("/home/jpt6/NCI/files_to_process") if
		isfile(join("/home/jpt6/NCI/files_to_process",
		files_to_process))]

	for files in files_to_be_processed :
		self.Files_to_Process_List.addItem(files)
 

	processed_files = [processed_files for processed_files in
		listdir("/home/jpt6/NCI/processed_files") if
		isfile(join("/home/jpt6/NCI/processed_files",
		processed_files))]

	for files in processed_files :
		self.Processed_Files_List.addItem(files)
 
        Upload_Button.clicked.connect(self.buttonClicked)            
        Match_Button.clicked.connect(self.buttonClicked)            
        ReIdentify_Button.clicked.connect(self.buttonClicked)            
        Quit_Button.clicked.connect(self.buttonClicked)
        
        self.statusBar()
        
        self.setGeometry(500, 300, 520, 400)
        self.setWindowTitle('Deduplication')
        self.show()
        
    def buttonClicked(self):
     
        sender = self.sender()
	if sender.text() == 'Upload Data':
		fl = open('login_info', 'r')
		id=fl.readline()
		id=id.rstrip()
		pwd=fl.readline()
		pwd=pwd.rstrip()
		fl.close()

		print self.Files_to_Process_List.currentItem().text()
		upload_file = self.Files_to_Process_List.currentItem().text()
		f = open('upload_tfile', 'w')
		st = os.stat('upload_tfile')
		os.chmod('upload_tfile', st.st_mode | stat.S_IEXEC)
		f.write('#!/bin/sh\n')
		f.write('scp "jpt6@thinktank:"'+upload_file+' .\n')
#		f.write("cat CCF_hashfile | perl -pe 's/\\x09/,/g' > CCF_hashfile.csv\n")
		f.write('upload_pats.pl files_to_process/'+upload_file+'\n')
		f.write('mv files_to_process/'+upload_file+' processed_files\n')
		f.close()
		self.Processed_Files_List.addItem(upload_file)
		for CSV_File_Name in self.Files_to_Process_List.selectedItems():
			self.Files_to_Process_List.takeItem(self.Files_to_Process_List.row(CSV_File_Name))

		self.statusBar().showMessage('Uploading file to DB...')
		os.system('upload_tfile')
		self.statusBar().showMessage('File uploaded.')
	if sender.text() == 'Quit':
		exit()
	if sender.text() == 'Re-Identify':
		self.statusBar().showMessage('Re-Identify Patient...')

		fl = open('login_info', 'r')
		id=fl.readline()
		id=id.rstrip()
		pwd=fl.readline()
		pwd=pwd.rstrip()
		fl.close()

		localUser=id
		localPassword=pwd
		localDatabase='Test'
		conn = pymysql.connect(host='icb-turnbull', user=localUser, password=localPassword, database=localDatabase)
		cursor=conn.cursor()

		print self.Master_ID_input.text()
		mid='89644'
		mid=self.Master_ID_input.text()
#		print "SELECT Master_ID, PAT_ID, LAST_NAME, FIRST_NAME, SSN, BIRTH_DATE, Hospital FROM hospital_lookup JOIN hashing_lookup_archive ON hashing_lookup_archive.HASHED_ID = hospital_lookup.HASHED_ID JOIN demographics_archive ON demographics_archive.PAT_ID = hashing_lookup_archive.LOCAL_ID WHERE Master_ID = '"+str(mid)+"';"
#		cursor.execute("SELECT Master_ID, PAT_ID, LAST_NAME, FIRST_NAME, SSN, BIRTH_DATE, Hospital FROM hospital_lookup JOIN hashing_lookup_archive ON hashing_lookup_archive.HASHED_ID = hospital_lookup.HASHED_ID JOIN demographics_archive ON demographics_archive.PAT_ID = hashing_lookup_archive.LOCAL_ID WHERE Master_ID = "+self.Master_ID_input.text()+";")
		cursor.execute("SELECT Master_ID, PAT_ID, LAST_NAME, FIRST_NAME, SSN, BIRTH_DATE, Hospital FROM hospital_lookup JOIN hashing_lookup_archive ON hashing_lookup_archive.HASHED_ID = hospital_lookup.HASHED_ID JOIN demographics_archive ON demographics_archive.PAT_ID = hashing_lookup_archive.LOCAL_ID WHERE Master_ID = '"+str(mid)+"';")
		conn.commit()
		pats_found = cursor.fetchall()
		for pats in pats_found :
			print pats

	if sender.text() == 'Match Data':
		fl = open('login_info', 'r')
		id=fl.readline()
		id=id.rstrip()
		pwd=fl.readline()
		pwd=pwd.rstrip()
		fl.close()
		f = open('match_tfile', 'w')
		st = os.stat('match_tfile')
		os.chmod('match_tfile', st.st_mode | stat.S_IEXEC)
		f.write('#!/bin/sh\n')
#		f.write('cd /home/jpt6/test_pats\n')
#		f.write('etl_ccf.pl\n')
#		f.write('cd /home/jpt6/test_pats\n')
#		self.statusBar().showMessage('Transforming data...')
		os.system('echo "use Test; delete from cluster_output;" | mysql --user='\
			+id+' --password='+pwd)
		os.system('echo "use Test; delete from clustered_stats;" | mysql --user='\
			+id+' --password='+pwd)
		f.write('cd /home/jpt6/Capricorn/matchupdate_12152015/Matching-cmd\\ ver1.0.1\n')
		f.write('python matcher.pyc config.mysql matching_ddlinput.mysql\n')
		f.close()
        	self.statusBar().showMessage('Matching data...')
		os.system('match_tfile')
        	self.statusBar().showMessage('Building Master Index...')

		localUser=id
		localPassword=pwd
		localDatabase='Test'
		conn = pymysql.connect(host='icb-turnbull', user=localUser, password=localPassword, database=localDatabase)
		cursor=conn.cursor()

		cursor.execute("SELECT * FROM cluster_output JOIN hospital_lookup on hospital_lookup.HASHED_ID = cluster_output.LacdrnPtID ORDER BY GPID;")
		conn.commit()
		last_GPID = ''

		icnt = 0
		matching_results = cursor.fetchall()
		for fields in matching_results :
			LacdrnPtID = fields[0]
			GPID = fields[1]

			if GPID != last_GPID :
				cursor.execute("INSERT INTO Master_ID VALUES ();")
				last_Master_ID = cursor.lastrowid
				cursor.execute("UPDATE hospital_lookup SET Master_ID = "+str(last_Master_ID)+" WHERE HASHED_ID = '"+LacdrnPtID+"';")
			else :
				cursor.execute("UPDATE hospital_lookup SET Master_ID = "+str(last_Master_ID)+" WHERE HASHED_ID = '"+LacdrnPtID+"';")
			conn.commit()
#			if (icnt % 10) == 0:
#				print str(icnt)+" UPDATE hospital_lookup SET Master_ID = "+str(last_Master_ID)+" WHERE HASHED_ID = '"+LacdrnPtID+"';"
			icnt = icnt + 1
			last_GPID = GPID;
		conn.close()

def main():
    
    app = QtGui.QApplication(sys.argv)
    match_win = Matching_MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

