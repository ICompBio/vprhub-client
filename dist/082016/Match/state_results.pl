#!/usr/bin/perl -w

#
#   Re-Identify Patients
#
use DBI;
use strict;

my $login_id;
my $passwrd;

open(my $LI, '<', "login_info")
    or die "Could not open login_info: $!\n";

$login_id = <$LI>;
$passwrd = <$LI>;
chomp $login_id;
chomp $passwrd;

my $dbh = DBI->connect('DBI:mysql:Test', $login_id, $passwrd);

my $sth = $dbh->prepare('use Test;');
$sth->execute();

my $sql_get_Master_IDs = "SELECT * FROM cluster_output GROUP BY GPID ORDER BY GPID;";

$sth = $dbh->prepare($sql_get_Master_IDs);
$sth->execute();

my $GPID;
my @MGPID;
my $multiplicity;
my @HASHED_ID;
my @Hospital;
my @Master_ID;
my $ii;
my $jj;

my $STATE_NAME;
my @states;
my $ic;

while (my $hash_ref = $sth->fetchrow_hashref()) {

	foreach my $key (keys (%{$hash_ref})) {

		if ($key eq "GPID") {

			$GPID = $hash_ref->{$key};

			my $sql_reident = "SELECT COUNT(*) AS multiplicity FROM cluster_output WHERE GPID = '$GPID'";

			my $sth2 = $dbh->prepare($sql_reident);
			$sth2->execute();
			while (my $hash_ref_reident = $sth2->fetchrow_hashref()) {

				foreach my $key_reident (keys (%{$hash_ref_reident})) {

					if ($key_reident eq "multiplicity") {
						$multiplicity = $hash_ref_reident->{$key_reident};
					}
				}
				if ($multiplicity > 1) {

					my $sql_get_multiple = "SELECT * FROM cluster_output JOIN hospital_lookup on hospital_lookup.HASHED_ID = cluster_output.LacdrnPtID WHERE GPID = '$GPID'";

					my $sth3 = $dbh->prepare($sql_get_multiple);
					$sth3->execute();
					my $mcnt = 0;
					while (my $hash_ref_multiples = $sth3->fetchrow_hashref()) {

						foreach my $key_multiple
								(keys (%{$hash_ref_multiples})) {

							if ($key_multiple eq "HASHED_ID") {
								$HASHED_ID[$mcnt] = $hash_ref_multiples->{$key_multiple};
							}
							if ($key_multiple eq "Hospital") {
								$Hospital[$mcnt] = $hash_ref_multiples->{$key_multiple};
							}
							if ($key_multiple eq "GPID") {
								$MGPID[$mcnt] = $hash_ref_multiples->{$key_multiple};
							}
							if ($key_multiple eq "Master_ID") {
								$Master_ID[$mcnt] = $hash_ref_multiples->{$key_multiple};
							}
						}
						$mcnt++;
					}
					for ($ii = 0; $ii < $mcnt; $ii++) {
						my $found = 0;
						$ic = 0;
						while ($found == 0 && $ic < scalar @states) {
							if ($Hospital[$ii] eq $states[$ic]) {
								$found = 1;
							} else {
								$ic++;
							}
						}
						if (not $found) {
							open ($STATE_NAME, '>',
								"return_files/".$Hospital[$ii]);
							print $STATE_NAME "<xml>\n";
							$states[scalar @states] = $Hospital[$ii];
						} else {
							open ($STATE_NAME, '>>',
								"return_files/".$Hospital[$ii]);
						}
						print $STATE_NAME "<Multiple_Patient>\n  ".
							"<Hospital>$Hospital[$ii]</Hospital>\n  ".
							"<Master_ID>$Master_ID[$ii]</Master_ID>\n  ".
							"<Hashed_ID>$HASHED_ID[$ii]</Hashed_ID>";
						for ($jj = 0; $jj < $mcnt; $jj++) {
							if ($jj != $ii) {
								print $STATE_NAME "\n    ".
								"<Also_at>$Hospital[$jj]".
								"</Also_at>";
							}
						}
						print $STATE_NAME "\n</Multiple_Patient>\n";
						close $STATE_NAME;
					}
				}
			}
		}
	}
}

for ($ic = 0; $ic < scalar @states; $ic++) {
	open ($STATE_NAME, '>>', "return_files/".$states[$ic]);
	print $STATE_NAME "</xml>\n";
	close $STATE_NAME;
}
