#!/usr/bin/perl -w

#
#  Creates MySQL matching_input table from the online (pseudo) random names
#  Written 09/21/2015
#  John P. Turnbull, PhD
#

use strict;
use DBI;
use File::Basename;

my $newline;

my $dbh;
my $sth;

use DBI;

my $ii;

$dbh = DBI->connect('DBI:mysql:Test', 'jpt6', '********');

$sth = $dbh->prepare('use Test;');
$sth->execute();

my $sql_clear_cluster_output = "DELETE FROM cluster_output;";
$sth = $dbh->prepare($sql_clear_cluster_output);
$sth->execute();

my $sql_clear_clustered_stats = "DELETE FROM clustered_stats;";
$sth = $dbh->prepare($sql_clear_clustered_stats);
$sth->execute();

my $line;
my $sql_insert_line;

my $filename = $ARGV[0];

open(my $fh, '<', $filename)
	or die "Could not open $filename: $!\n";

$line = <$fh>;		# Skip Header line.
my $icnt = 0;
while ($line = <$fh>) {
	chomp $line;
	my @fields = split ",",$line;
#		$fields[0] = $icnt++;
	$sql_insert_line = "INSERT INTO matching_input (".
        		"LacdrnPtID,".
        		"FNLNDOB,".
        		"LNFNDOB,".
        		"FNLNBOD,".
        		"FNSSN,".
        		"LNSSN,".
        		"3LN_FNLNDOB,".
        		"3LN_LNFNDOB,".
        		"3LN_FNLNBOD,".
        		"3LN_FNSSN,".
        		"3LN_LNSSN,".
        		"SX_FNLNDOB,".
        		"SX_LNFNDOB,".
        		"SX_FNLNBOD,".
        		"SX_FNSSN,".
        		"SX_LNSSN,".
        		"DOBSSN,".
        		"SSN,".
        		"GENDER,".
		"FLAG,".
		"PARTNERID".
	") VALUES (";
	for ($ii = 0; $ii < scalar @fields - 1; $ii++) {
		$sql_insert_line = $sql_insert_line.(length $fields[$ii] == 0 ? '\'\'' : $dbh->quote($fields[$ii])).",";
	}
	$sql_insert_line = $sql_insert_line.((length $fields[scalar @fields - 1] == 0 || $fields[scalar @fields - 1] eq '\n') ? '\'\'' : $dbh->quote($fields[scalar @fields - 1])).",'0',".$icnt++.");";
	print $sql_insert_line."\n";
	$sth = $dbh->prepare($sql_insert_line);
	$sth->execute();

	my @parsed_filename = split "_",fileparse($filename);
	$sql_insert_line = "INSERT INTO hospital_lookup (".
		"HASHED_ID,".
		"Hospital".
	") VALUES ('".
		$fields[0]."','".
		$parsed_filename[0].
	"');";
	print $sql_insert_line."\n";
	$sth = $dbh->prepare($sql_insert_line);
	$sth->execute();

}
