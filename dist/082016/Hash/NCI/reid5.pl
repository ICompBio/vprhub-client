#!/usr/bin/perl -w

use strict;
use DBI;
use XML::Parser;
use XML::Twig;
#use File::Slurp;


my $state_file;
my $myline;

my $hashed_id;
my $tline;
my $infile;

if ($#ARGV >= 0) {
	$infile = $ARGV[0];
} else {
	print "No file name given\n";
	exit();
}

my $auth_str = <STDIN>;
#print "auth str $auth_str\n";
my @auth_parts = split " ", $auth_str;
my $db_id = $auth_parts[0];
my $pwd = $auth_parts[1];
chomp $db_id;
chomp $pwd;

my $dbh = DBI->connect('DBI:mysql:Test', $db_id, $pwd);

my $sth = $dbh->prepare('use Test;');
$sth->execute();

my @states;
my $ic;

my $last_name;
my $first_name;
my $pat_id;
my $birth_date;
my $matching_state;
my @matching_states;
my $msinx;

my %stats;

$msinx = 0;

open (my $fdstate, '<', 'mystate')
	or die "Could not open state file: $!\n";

open (my $fdout, '>', 'rematched.csv')
	or die "Could not open rematched file: $!\n";

my $mystate = <$fdstate>;
chomp $mystate;

print "infile: $infile\n";

#my $state_file_lines = read_file($infile);

my $state_file_lines="";
my $oneline;
my $fdin;

open($fdin, '<', $infile);
while ($oneline = <$fdin>) {
	$state_file_lines = $state_file_lines.$oneline;
}
close($fdin);

my $twig = XML::Twig->new();

$twig->parse($state_file_lines);

foreach my $multipat ($twig->root->children('Multiple_Patient')) {

	$hashed_id = $multipat->first_child('Hashed_ID')->text();
	$hashed_id =~ s/\S*?(\w*)/$1/gm;
	$hashed_id =~ s/\v//gm;

	print $hashed_id."\n";

	my $sql_re_identify = "SELECT * FROM hashing_lookup_archive".
		" JOIN demographics_archive on demographics_archive.PAT_ID =".
		" hashing_lookup_archive.LOCAL_ID WHERE".
		" hashing_lookup_archive.HASHED_ID = '$hashed_id';";

	$sth = $dbh->prepare($sql_re_identify);
	$sth->execute();

	while (my $hash_ref = $sth->fetchrow_hashref()) {

		foreach my $key (keys (%{$hash_ref})) {

			if ($key eq "PAT_ID") {
				$pat_id = $hash_ref->{$key};
			}
			if ($key eq "LAST_NAME") {
				$last_name = $hash_ref->{$key};
			}
			if ($key eq "FIRST_NAME") {
				$first_name = $hash_ref->{$key};
			}
			if ($key eq "BIRTH_DATE") {
				$birth_date = $hash_ref->{$key};
			}
		}

		print "$pat_id, $last_name, $first_name, $birth_date\n";
		print $fdout $pat_id, $last_name, $first_name, $birth_date;
	}

	foreach my $other_states ($multipat->children('Also_at')) {
		print "Other states: ".$other_states->text()."\n";
		print $fdout ", ".$other_states->text();
	}
	print $fdout "\n";
}

close $fdout;
