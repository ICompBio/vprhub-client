#!/usr/bin/python
# -*- coding: utf-8 -*-

"""

Written for The Nation Cancer Institute SEER project
by The Institute for Computational Biology at
Case Western Reserve University.
John P. Turnbull, Ph.D.
June 06, 2016.

"""

import sys
import os
import stat
#import pymssql
#import pymysql
import datetime

import Tkinter, tkFileDialog

from PyQt4 import QtGui, QtCore

from Tkinter import *

class Example(QtGui.QMainWindow):
    
    def __init__(self):
        super(Example, self).__init__()
        try:
            stf = open('mystate', 'r')
            self.mystate = stf.readline()
            self.mystate = self.mystate.rstrip()
        except:
            self.initStates()

        self.getUserPwd()

        self.initUI()

    def initStates(self):

        print 'Opening state'
        stf = open('state_abbreviations.txt', 'r')
        allstates = stf.readlines()
        states = {}
        statesname = []
        statesabreve = []
        statesnumber = []
        icnt=1;
        for onestate in allstates:
            onestate = onestate.rstrip()
            a = onestate.split('	')
            states.update({a[0]:a[1]})
            statesname.append(a[0])
            statesabreve.append(a[1])
            statesnumber.append(icnt)
            icnt += 1

        state_selection_win = Tk()
        state_selection_win.geometry("250x150+300+300")
        state_selection_win.title("State Selection")

        var = StringVar(state_selection_win)
        var.set("Select a State")

        def grab_and_assign(event):
            self.mystate = var.get()
            label_chosen_variable= Label(state_selection_win, text=self.mystate)
            label_chosen_variable.grid(row=1, column=2)
            print mystate

        dropmenu = apply(OptionMenu, (state_selection_win, var) + tuple(statesname))
        dropmenu.pack()

        def Save_Callback():
            state_fd = open('mystate', 'w')
            self.mystate  = var.get()
            print self.mystate
            inx = statesname.index(self.mystate)
            state_fd.write(states[self.mystate] + '\n')
            state_fd.write(statesname[inx] + '\n')
            state_fd.write(str(statesnumber[inx]) + '\n')
            state_fd.close()
            state_selection_win.destroy()

        Save_Button = Tkinter.Button(state_selection_win, text="Save", command=Save_Callback)
        Save_Button.pack()

        mainloop()

    def getUserPwd(self):

        def enter_Auth(parent, caption, width=None, **options):
            Label(parent, text=caption).pack(side=TOP)
            entry = Entry(parent, **options)
            if width:
                entry.pack(side=TOP, padx=10, fill=BOTH)
            return entry

        def enter(event):
            check_password()

        def check_password():
            uid = user.get()
            pwd = password.get()

            config_fd = open('hashupdate_12152015/Hashing-cmd ver1.0.1/config.mysql', 'w')
            config_fd.write('user=' + uid + '\n')
            config_fd.write('password=' + pwd + '\n')
            config_fd.write('projectid=Test\n')
            config_fd.write('dbusername1=nci\n')
            config_fd.write('dbpassword1=**********\n')
            config_fd.write('dbusername2=nci\n')
            config_fd.write('dbpassword2=**********\n')
            config_fd.write('host1=localhost\n')
            config_fd.write('host2=localhost\n')
            config_fd.write('port1=3306\n')
            config_fd.write('port2=3306\n')
            config_fd.write('database1=Test\n')
            config_fd.write('database2=Test\n')
            config_fd.write('dbusername=nci\n')
            config_fd.write('dbpassword=**********\n')
            config_fd.write('host=localhost\n')
            config_fd.write('port=3306\n')
            config_fd.write('database=Test\n')
            config_fd.write('srctable=hashing_lookup\n')
            config_fd.write('targettable=HASHING_OUTPUT\n')
            config_fd.write('config=2\n')
            config_fd.write('process=2\n')
            config_fd.write('server=1\n')
            config_fd.write('instance=\n')
            config_fd.write('instance1=\n')
            config_fd.write('instance2=\n')
            config_fd.write('integrated security=\n')
            config_fd.write('integrated security1=\n')
            config_fd.write('integrated security2=\n')
            config_fd.close()

            UserPwd_win.destroy()
            return

        UserPwd_win = Tk()
        UserPwd_win.geometry("250x150+300+300")
        UserPwd_win.title("Authentication")

        parent = Frame(UserPwd_win, padx=10, pady=10)
        parent.pack(fill=BOTH, expand=TRUE)
        user = enter_Auth(parent, "User Name:", 16, show='*')
        password = enter_Auth(parent, "Password:", 16, show="*")
        save_button = Button(parent, borderwidth=4, text="Sign in", width=10, pady=8, command=check_password)
        save_button.pack(side=BOTTOM)
        password.bind = ('<Return>', enter)
        user.focus_set()


#        def Save_Callback():
#            UserPwd_win.destroy()

#        Save_Button = Tkinter.Button(UserPwd_win, text="Save", command=Save_Callback)
#        Save_Button.pack()

        mainloop()

    def initUI(self):      

        Upload_Button = QtGui.QPushButton("Upload Data", self)
        Upload_Button.move(30, 50)

        Re_Ident_Button = QtGui.QPushButton("Re-Identify", self)
        Re_Ident_Button.move(160, 50)

        Quit_Button = QtGui.QPushButton("Quit", self)
        Quit_Button.move(290, 50)

        Upload_Button.clicked.connect(self.buttonClicked)            
        Re_Ident_Button.clicked.connect(self.buttonClicked)
        Quit_Button.clicked.connect(self.buttonClicked)
        
        self.statusBar()
        
        self.setGeometry(300, 300, 420, 150)
        self.setWindowTitle(self.mystate + ' Hashing')
        self.show()
        
    def buttonClicked(self):
      
        sender = self.sender()
	print sender.text() + ' was pressed'
	if sender.text() == 'Quit':
		exit()
	if sender.text() == 'Upload Data':

		id='nci'
		pwd='**********'
		host='localhost'

		os.system('echo "USE Test; DELETE FROM demographics;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\'')

		dir_browser = Tkinter.Tk()
		filename = tkFileDialog.askopenfilename(parent=dir_browser,initialdir="/cygdrive/c",title='Choose a file')
		print 'file name is ' + filename

		if filename[-4:] == '.xml':
			f = open(self.mystate + '_hash_cmds', 'w')
			st = os.stat(self.mystate + '_hash_cmds')
			f.write('#!/bin/sh\n')
#			f.write('cd /home/jpt6/test_pats\n')
			f.write('echo nci **********| ./etlnci.pl ' + filename + ' ' + os.path.basename(__file__) + '\n')
			print 'etlnci.pl ' + filename + ' ' + os.path.basename(__file__) + '\n'
        		self.statusBar().showMessage('Uploading data...')
			os.chmod(self.mystate + '_hash_cmds', st.st_mode | stat.S_IEXEC)
			f.close()
			os.system('./'+self.mystate + '_hash_cmds')
        		self.statusBar().showMessage('Uploaded data.')
		dir_browser.destroy()
	
		f = open(self.mystate + '_hash_cmds', 'w')
		f.write('cd hashupdate_12152015/Hashing-cmd\\ ver1.0.1\n')
		f.write('java -jar Hashingv1.0.1.jar config.mysql ddlinput.mysql\n')
		f.close()

		os.system('echo "USE Test; DELETE FROM HASHING_OUTPUT;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\'')
		os.system('echo "USE Test; DELETE FROM hashing_lookup;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\'')
		os.system('echo "USE Test; DELETE FROM HASHING_OUTPUT_ARCHIVE;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\'')
		os.system('echo "USE Test; DELETE FROM hashing_lookup_archive;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\'')

        	self.statusBar().showMessage('Hashing data...')
		os.system('./'+self.mystate + '_hash_cmds')
		self.statusBar().showMessage('Data hashed.')

		f = open(self.mystate + '_hash_cmds', 'w')
		st = os.stat(self.mystate + '_hash_cmds')
		os.chmod(self.mystate + '_hash_cmds', st.st_mode | stat.S_IEXEC)
		f.write('#!/bin/sh\n')
		this_day_hash = self.mystate + '_hash_'+str(datetime.date.today())
		this_day_lookup = self.mystate + '_lookup_'+str(datetime.date.today())
		self.statusBar().showMessage('Generating csv files....')
		os.system('echo "use Test; select * from HASHING_OUTPUT;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\' > '+this_day_hash)
		f.write('cat '+this_day_hash+\
			' | perl -pe "s/\\x09/,/g" > files_to_process/'+
			this_day_hash+'.csv\n')
		os.system('echo "use Test; insert into HASHING_OUTPUT_ARCHIVE select * from HASHING_OUTPUT;" | mysql --host=localhost --user='\
			+id+' --password=\''+pwd+'\'')

		os.system('echo "use Test; select * from hashing_lookup;" | mysql --host='\
			+host+' --user='\
			+id+' --password=\''+pwd+'\' > '+this_day_lookup)
		f.write('cat '+this_day_lookup+\
			' | perl -pe "s/\\x09/,/g" > files_to_process/'+
			this_day_lookup+'.csv\n')
		os.system('echo "use Test;'\
			' insert into hashing_lookup_archive select * from hashing_lookup;"'\
			' | mysql --host='+host+' --user='\
			+id+' --password=\''+pwd+'\'')
		os.system('echo "use Test;'\
			' insert into demographics_archive select * from demographics;"'\
			' | mysql --host='+host+' --user='\
			+id+' --password=\''+pwd+'\'')
		f.close()

		os.system('"/cygdrive/c/Program Files/Internet Explorer/iexplore" "vprhub.icompbio.net?state='+self.mystate+'&upload=true"')
		os.system('"/cygdrive/c/Program Files (x86)/Internet Explorer/iexplore" "vprhub.icompbio.net?state='+self.mystate+'&upload=true"')

		self.statusBar().showMessage('Transmitting Data....')
		os.system('./'+self.mystate + '_hash_cmds')
		self.statusBar().showMessage('Data Transmitted.')

#		localHostName='localhost'
#		localUser=id
#		localPassword=pwd
#		localDatabase='Test'
#		conn = pymysql.connect(host='icb-turnbull', user=localUser, password=localPassword, database=localDatabase)
#		cursor=conn.cursor()
#		cursor.execute("DELETE FROM hospital_lookup")
#		cursor.execute("INSERT INTO hospital_lookup (HASHED_ID, hospital) SELECT HASHED_ID, 'CCF' AS Hospital FROM hashing_lookup")
#		cursor.execute("INSERT INTO matching_input (LacdrnPtID, FNLNDOB, LNFNDOB, FNLNBOD, FNSSN, LNSSN, 3LN_FNLNDOB, 3LN_LNFNDOB, 3LN_FNLNBOD, 3LN_FNSSN, 3LN_LNSSN, SX_FNLNDOB, SX_LNFNDOB, SX_FNLNBOD, SX_FNSSN, SX_LNSSN, DOBSSN, SSN, GENDER, FLAG, PARTNERID) SELECT HASHED_ID AS LacdrnPtID,  FNLNDOB, LNFNDOB, FNLNBOD, FNSSN, LNSSN, 3LN_FNLNDOB, 3LN_LNFNDOB, 3LN_FNLNBOD, 3LN_FNSSN, 3LN_LNSSN, SX_FNLNDOB, SX_LNFNDOB, SX_FNLNBOD, SX_FNSSN, SX_LNSSN, DOBSSN, SSN, GENDER, 0 AS FLAG, '' AS PARTNERID FROM HASHING_OUTPUT")
#		conn.commit()
#		conn.close()
#        	self.statusBar().showMessage('Data Transmitted and hashed_ids Copied')

	if sender.text() == 'Re-Identify':

		dir_browser = Tkinter.Tk()
		filename = tkFileDialog.askopenfilename(parent=dir_browser,initialdir="/cygdrive/c",title='Choose a file')
		print 'file name is ' + filename

		self.statusBar().showMessage('Re-Identifying Data...')
		os.system("echo nci ********** | ./reid5.pl "+filename)
		self.statusBar().showMessage('Data Re-Identified.')

def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

