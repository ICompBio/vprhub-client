#!/usr/bin/perl -w

#
#  This program reads and parses an XML file containing the EMR
#  patient data and uploads to the HealthLnk database in preparation
#  of hashing the patient data.  The hashed data can then be sent
#  to a central (public) site with hashed data from other locations
#  where it can then search for matches of patients among the
#  various locations.
#
#  Written for The Nation Cancer Institute SEER project
#  by The Institute for Computational Biology at
#  Case Western Reserve University.
#  John P. Turnbull, Ph.D.
#  June 06, 2016.
#

use strict;
use DBI;

my $infile;

$infile = "IDnaaccr.xml";

my $outfile;

$outfile = "CCF_ETL.csv";

my $offset = 0;

if ($#ARGV >= 0) {
  $infile = $ARGV[0];
#  if ($#ARGV >= 1) {
#    $outfile = $ARGV[1];
#  }
  if ($ARGV[1] eq 'CCF.py') {
	$offset = 1000000;
  }
}

my $statefile_name='mystate';
if (open (my $sfn, $statefile_name)) {
	my $state_abbrev = <$sfn>;
	chomp $state_abbrev;
	my $state_name = <$sfn>;
	chomp $state_name;
	my $state_inx = <$sfn>;
	chomp $state_inx;
	$offset = $state_inx * 10000000;
} else {
	print "Could not open mystate file\n";
	exit();
}

my $auth_str = <STDIN>;
my @auth_parts = split " ", $auth_str;
my $db_id = $auth_parts[0];
my $pwd = $auth_parts[1];
chomp $db_id;
chomp $pwd;

#  Run option 1:  Create .csv output file
#  Run option 2:  upload directly to database

my $run_option = 2;
 
my $dbh;
my $sth;
my $sql_add_pat;

my $outline;

if ($run_option == 1) {

	open($outline, '>', $outfile) or die "Could not open output file '$outfile' $!\n";

} elsif ($run_option == 2) {

	use DBI;

	$dbh = DBI->connect('DBI:mysql:Test;host=localhost', $db_id, $pwd);

	$sth = $dbh->prepare('use Test;');
	$sth->execute();

}

my $id=1;

my $first_name = "";
my $last_name = "";
my $ssn = "";
my $dob = "";
my $zip = "";
my $gender = "";
my @dob_parts;
my @fields;

my $fd;
open ($fd, '<', $infile) or die "Could not open $infile $!";

my $inpat = 0;
my $myline;

while ($myline = <$fd>) {

	if ($inpat == 0) {
		if ($myline =~ m/<Patient>/) {
			$inpat = 1;
		}
	} else {
		if ($myline =~ m/<\/Patient>/) {

			print "$first_name, $last_name, $ssn, $gender\n";
			print "=======================\n";
		
		    if (length $dob == 0) {
		      $sql_add_pat = "INSERT INTO demographics (".
		        "PAT_ID,".
		        "FIRST_NAME,".
		        "LAST_NAME,".
		        "SSN,".
		        "BIRTH_DATE,".
		        "BMONTH,".
		        "BYEAR,".
		        "GENDER".
		      ") VALUES (".
		        "$id+$offset,".
		        $dbh->quote($first_name).",".
		        $dbh->quote($last_name).",".
		        $dbh->quote($ssn).",".
		        "NULL,".
		        "NULL,".
		        "NULL,".
		        $dbh->quote($gender).
		      ");";
		    } else {
		      $sql_add_pat = "INSERT INTO demographics (".
		        "PAT_ID,".
		        "FIRST_NAME,".
		        "LAST_NAME,".
		        "SSN,".
		        "BIRTH_DATE,".
				"GENDER".
		      ") VALUES (".
		        "$id+$offset,".
		        $dbh->quote($first_name).",".
		        $dbh->quote($last_name).",".
		        $dbh->quote($ssn).",".
		        $dbh->quote($dob).",".
				$dbh->quote($gender).
		      ");";
		    }
		      print $sql_add_pat."\n";
		    if (($id % 10) == 0) {
		      print $sql_add_pat."\n";
		   }
		
		    $sth = $dbh->prepare($sql_add_pat);
		    $sth->execute();
		
		    $id++;
			$inpat = 0;
		} else {
			if ($myline =~ m/nameFirst/) {
				$first_name = $myline;
				$first_name =~ s/.*?>(.*?)<.*?$/$1/;
				$first_name =~ s/\v//g;
				$first_name = uc $first_name;
				print "First name: $first_name\n";
			} elsif ($myline =~ m/nameLast/) {
				$last_name = $myline;
				$last_name =~ s/.*?>(.*?)<.*?$/$1/;
				$last_name =~ s/\v//g;
				$last_name = uc $last_name;
			} elsif ($myline =~ m/socialSecurityNumber/) {
				$ssn = $myline;
				$ssn =~ s/.*?>(.*?)<.*?$/$1/;
				$ssn =~ s/\v//g;
			} elsif ($myline =~ m/dateOfBirth/) {
				$dob = $myline;
				$dob =~ s/.*?>(.*?)<.*?$/$1/;
				$dob =~ s/\v//g;
				$dob =~ s/\s//g;
				if (length($dob) == 8) {
					$dob =~ s/(\d\d\d\d)(\d\d)(\d\d)/$1-$2-$3/;
				} else {
					$dob = "";
				}
			} elsif ($myline =~ m/sex/) {
				$gender = $myline;
				$gender =~ s/.*?>(.*?)<.*?$/$1/;
				$gender =~ s/\v//g;
				if ($gender == 1) {
					$gender = 'male';
				} elsif ($gender == 2) {
					$gender = 'female';
				} else {
					$gender = 'un';
				}
			}
		}
	}

}
